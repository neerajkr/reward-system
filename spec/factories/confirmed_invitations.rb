FactoryBot.define do
  factory :confirmed_invitation do
    invited_by { 'C' }
    accepted_by { 'B' }
    invited_at { '2018-06-12 09:41'.to_time }

    initialize_with { new(invited_by: invited_by, accepted_by: accepted_by, invited_at: invited_at) }
  end
end
