FactoryBot.define do
  factory :recommended_invitation do
    invited_by { 'A' }
    invited_to { 'B' }
    invited_at { '2018-06-12 09:41'.to_time }

    initialize_with { new(invited_by: invited_by, invited_to: invited_to, invited_at: invited_at) }
  end
end
