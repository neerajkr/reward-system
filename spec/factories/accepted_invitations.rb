FactoryBot.define do
  factory :accepted_invitation do
    accepted_by { 'B' }
    accepted_at { '2018-06-14 09:41'.to_time }

    initialize_with { new(accepted_by: accepted_by, accepted_at: accepted_at) }
  end
end
