require 'rails_helper'

RSpec.describe AcceptedInvitation, type: :model do

  describe described_class.new(accepted_at: '2018-06-12 09:41'.to_time, accepted_by: 'A') do
    it { is_expected.to have_attributes(:accepted_at => '2018-06-12 09:41'.to_time) }
    it { is_expected.to have_attributes(:accepted_by => 'A') }
  end

end
