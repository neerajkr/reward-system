require 'rails_helper'

RSpec.describe AcceptedInvitationSoFar, type: :model do

  describe '#init' do

    subject { described_class.init }

    it 'initializes accepted invitations' do
      expect(subject).to be_empty
      expect(subject).to be_a_kind_of(Array)
      expect(described_class.accepted_invitations).not_to be_nil
    end
  end

  describe '#all' do

    subject { described_class.all }

    context 'when no invitation accepted' do

      it 'returns empty array' do
        expect(subject).to be_a_kind_of(Array)
        expect(subject).to be_empty
      end

    end

    context 'when some invitations accepted' do
      let!(:accepted_invitation) { build(:accepted_invitation) }

      before { described_class.add(accepted_invitation) }

      it 'returns the array of accepted invitations so far' do
        expect(subject).not_to be_empty
        expect(subject).to include(accepted_invitation)
      end
    end
  end

  describe '#add' do
    let!(:accepted_invitation) { build(:accepted_invitation) }

    subject { described_class.add(accepted_invitation) }

    it 'adds invitation into accepted invitations so far' do
      subject
      expect(described_class.all).to include(accepted_invitation)
    end
  end
end
