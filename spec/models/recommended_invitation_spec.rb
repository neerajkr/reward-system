require 'rails_helper'

RSpec.describe RecommendedInvitation, type: :model do

  describe described_class.new(invited_by: 'A', invited_to: 'B', invited_at: '2018-06-12 09:41'.to_time) do
    it { is_expected.to have_attributes(:invited_by => 'A') }
    it { is_expected.to have_attributes(:invited_to => 'B') }
    it { is_expected.to have_attributes(:invited_at => '2018-06-12 09:41'.to_time) }
  end

  describe '#accepted?' do
    let!(:recommended_invitation) { build(:recommended_invitation) }

    subject { recommended_invitation.accepted? }

    before { AcceptedInvitationSoFar.init }

    context 'when AcceptedInvitationSoFar has the record' do
      let!(:accepted_invitation) { build(:accepted_invitation) }

      before { AcceptedInvitationSoFar.add(accepted_invitation) }

      it 'returns true' do
        expect(subject).to be_truthy
      end
    end

    context 'when AcceptedInvitationSoFar is not having the record' do
      it 'returns false' do
        expect(subject).to be_falsey
      end
    end
  end

end
