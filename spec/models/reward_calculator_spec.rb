require 'rails_helper'

RSpec.describe RewardCalculator, type: :model do

  describe '#calculate' do

    subject { RewardCalculator.new(file: input_file).calculate }

    context 'when input file doesnt exist' do
      let!(:input_file) { "" }

      it 'returns error' do
        expect { subject }.to raise_error(Errno::ENOENT, /No such file or directory/)
      end
    end

    context 'when input file found' do
      let!(:tempfile) { Tempfile.new('input') }
      let!(:input_file) { tempfile.path }

      before do
        tempfile.write("2018-06-12 09:41 A recommends B\n")
        tempfile.write("2018-06-14 09:41 B accepts\n")
        tempfile.write("2018-06-16 09:41 B recommends C\n")
        tempfile.write("2018-06-17 09:41 C accepts\n")
        tempfile.write("2018-06-19 09:41 C recommends D\n")
        tempfile.write("2018-06-23 09:41 B recommends D\n")
        tempfile.write("2018-06-25 09:41 D accepts")

        tempfile.close
      end

      after { tempfile.unlink }

      it 'returns hash of rewards' do
        expect(subject).not_to be_empty
        expect(subject).to include('A' => 1.75, 'B' => 1.5, 'C' => 1.0)
      end
    end
  end
end
