require 'rails_helper'

RSpec.describe ConfirmedInvitation, type: :model do

  describe described_class.new(invited_by: 'A', accepted_by: 'B', invited_at: '2018-06-12 09:41'.to_time) do
    it { is_expected.to have_attributes(:invited_by => 'A') }
    it { is_expected.to have_attributes(:accepted_by => 'B') }
    it { is_expected.to have_attributes(:invited_at => '2018-06-12 09:41'.to_time) }
  end

  describe '#init' do

    subject { described_class.init }

    it 'initializes confirmed_invitations' do
      expect(subject).not_to be_nil
      expect(subject).to be_a_kind_of(Array)
      expect(subject).to be_empty
    end
  end

  describe '#all' do

    subject { described_class.all }

    context 'when no confirmed invitation present' do
      it 'returns empty array' do
        expect(subject).to be_a_kind_of(Array)
        expect(subject).to be_empty
      end
    end

    context 'when confirmed invitation present' do
      let!(:recommended_invitation) { build(:recommended_invitation) }

      before do
        described_class.init
        described_class.add(recommended_invitation)
      end

      it 'returns the array of confirmed invitations' do
        expect(subject).not_to be_empty
        expect(subject).to be_a_kind_of(Array)
        expect(subject.count).to eq(1)
        expect(subject[0].invited_by).to eq(recommended_invitation.invited_by)
        expect(subject[0].accepted_by).to eq(recommended_invitation.invited_to)
        expect(subject[0].invited_at).to eq(recommended_invitation.invited_at)
      end
    end
  end

  describe '#delete' do
    let!(:recommended_invitation) { build(:recommended_invitation) }

    before do
      described_class.init
      described_class.add(recommended_invitation)
    end

    subject { described_class.all.first.delete }

    it 'deletes confirmed invitation' do
      confirmed_invitation = described_class.all.first
      subject
      expect(described_class.all).not_to include(confirmed_invitation)
      expect(described_class.all).to be_empty
    end
  end

  describe '#add' do
    let!(:recommended_invitation) { build(:recommended_invitation, invited_at: '2018-06-12 09:41'.to_time + 1.day) }

    before { described_class.init }

    subject { described_class.add(recommended_invitation) }

    context 'when nobody invited before' do
      it 'creates a new object of confirmed invitation and into confirmed invitations array' do
        expect(subject).not_to be_empty
        expect(subject[0].invited_by).to eq(recommended_invitation.invited_by)
        expect(subject[0].accepted_by).to eq(recommended_invitation.invited_to)
        expect(subject[0].invited_at).to eq(recommended_invitation.invited_at)
      end

    end

    context 'when someone else invited before' do
      let!(:recommended_invitation2) { build(:recommended_invitation, invited_by: 'C') }

      before { described_class.add(recommended_invitation2) }

      it 'deletes new confirmed invitations and add a new object' do
        expect(described_class.all.first.invited_by).to eq(recommended_invitation2.invited_by)
        expect(described_class.all.first.accepted_by).to eq(recommended_invitation2.invited_to)
        expect(described_class.all.first.invited_at).to eq(recommended_invitation2.invited_at)

        subject

        expect(subject[0].invited_by).to eq(recommended_invitation2.invited_by)
        expect(subject[0].accepted_by).to eq(recommended_invitation2.invited_to)
        expect(subject[0].invited_at).to eq(recommended_invitation2.invited_at)
        expect(subject[0].invited_by).not_to eq(recommended_invitation.invited_to)
        expect(subject[0].invited_at).not_to eq(recommended_invitation.invited_at)
      end

    end
  end
end
