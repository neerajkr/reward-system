require 'rails_helper'

RSpec.describe InputParser, type: :model do

  describe '#parse' do
    let!(:tempfile) { Tempfile.new('input') }
    let!(:input_file) { tempfile.path }

    subject { described_class.new(file: input_file).parse }

    after { tempfile.unlink }

    context 'when input file doesnt exist' do
      let!(:input_file) { "" }

      it 'returns error' do
        expect { subject }.to raise_error(Errno::ENOENT, /No such file or directory/)
      end
    end

    context 'when time is not in correct format' do
      before do
        tempfile.write("2018-06-12 09:41 A recommends B\n")
        tempfile.write("invalid-time-format B accepts")
        tempfile.close
      end

      it 'raise error' do
        expect { subject }.to raise_error(InputParser::TimeFormatError)
      end
    end

    context 'when invited at time is in future' do
      before do
        tempfile.write("2018-06-12 09:41 A recommends B\n")
        tempfile.write("#{(Time.now + 1.hour).strftime("%Y-%m-%d %H:%M")} B accepts")
        tempfile.close
      end

      it 'raise error' do
        expect { subject }.to raise_error(InputParser::InvalidTimeError)
      end
    end

    context 'when file not having valid content' do
      before do
        tempfile.write("2018-06-12 09:41 A not-recommends B\n")
        tempfile.write("2018-06-14 09:41 B not-accepts")
        tempfile.close
      end

      it 'raise error' do
        expect { subject }.to raise_error(InputParser::ParsingError)
      end
    end

    context 'when input file found' do
      before do
        tempfile.write("2018-06-12 09:41 A recommends B\n")
        tempfile.write("2018-06-14 09:41 B accepts")
        tempfile.close
      end

      it 'returns data' do
        expect(subject).not_to be_empty
        recommended_invitation, accepted_invitation = subject
        expect(recommended_invitation).to be_a_kind_of(RecommendedInvitation)
        expect(recommended_invitation.invited_by).to eq('A')
        expect(recommended_invitation.invited_to).to eq('B')
        expect(recommended_invitation.invited_at).to eq('2018-06-12 09:41:00.000000000 +0530')
        expect(accepted_invitation).to be_a_kind_of(AcceptedInvitation)
        expect(accepted_invitation.accepted_by).to eq('B')
        expect(accepted_invitation.accepted_at).to eq('2018-06-14 09:41:00.000000000 +0530')
      end
    end
  end

end
