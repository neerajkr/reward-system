require 'rails_helper'

RSpec.describe RewardsController, type: :controller do

  describe '.create' do

    subject { post :create, { format: :json, params: params } }

    context 'when no file provided' do
      let!(:params) { {} }

      it 'returns http status as 404' do
        expect(subject).to have_http_status(404)
      end

      it 'returns response that no file found' do
        response = JSON.parse(subject.body)
        expect(response).to include('error' => 'File not found')
      end
    end

    context 'when valid file params provided' do
      let!(:file) do
        tempfile = Tempfile.new('input')
        tempfile.write("2018-06-12 09:41 A recommends B\n")
        tempfile.write("2018-06-14 09:41 B accepts\n")
        tempfile.write("2018-06-16 09:41 B recommends C\n")
        tempfile.write("2018-06-17 09:41 C accepts\n")
        tempfile.write("2018-06-19 09:41 C recommends D\n")
        tempfile.write("2018-06-23 09:41 B recommends D\n")
        tempfile.write("2018-06-25 09:41 D accepts")

        tempfile.close
        Rack::Test::UploadedFile.new(tempfile.path)
      end
      let!(:params) { { file: file } }

      after { file.unlink }


      it 'returns http status as 200' do
        expect(subject).to have_http_status(200)
      end

      it 'returns rewards in JSON format' do
        response = JSON.parse(subject.body)
        expect(response).to include('A' => 1.75, 'B' => 1.5, 'C' => 1.0)
      end
    end

  end

end
