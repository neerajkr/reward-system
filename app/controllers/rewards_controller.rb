class RewardsController < ApplicationController

  def create
    respond_to do |format|
      format.json do
        @rewards =  RewardCalculator.new(file: rewards_params[:file].tempfile).calculate if rewards_params[:file].present? && rewards_params[:file].tempfile.present?

        if @rewards
          render json: @rewards, status: :ok
        else
          render json: { error: 'File not found' }, status: 404
        end
      rescue => e
        render json: { error: [e.to_s] }, status: :unprocessable_entity
      end
    end
  end

  private

  def rewards_params
    params.permit(:file)
  end
end
