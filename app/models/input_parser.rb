class InputParser

  attr_reader :data

  def initialize(file:)
    @file = file
    @data = []
  end

  def parse
    File.open(@file, 'r') do |file|
      file.each do |line|
        invitation = line.delete("\n").split(' ')

        unless invitation.empty?
          validate_invitation(invitation)

          @data <<
            case invitation[3].to_sym
            when :recommends
              RecommendedInvitation.new(
                invited_at: invited_at(invitation),
                invited_by: invitation[2],
                invited_to: invitation[4],
              )
            when :accepts
              AcceptedInvitation.new(
                accepted_at: invited_at(invitation),
                accepted_by: invitation[2],
              )
          end
        end
      end
    end

    @data
  end

  class TimeFormatError < Exception; end
  class InvalidTimeError < Exception; end
  class ParsingError < Exception; end

  private

  def validate_invitation(invitation)
    invited_at = invited_at(invitation)
    raise TimeFormatError unless invited_at
    raise InvalidTimeError if invited_at > Time.now
    raise ParsingError unless %w(recommends accepts).include?(invitation[3])
  end

  def invited_at(invitation)
    "#{invitation[0]} #{invitation[1]}".to_time
  end
end
