class AcceptedInvitation

  attr_reader :accepted_at, :accepted_by

  def initialize(accepted_at: , accepted_by:)
    @accepted_at = accepted_at
    @accepted_by = accepted_by
  end

end
