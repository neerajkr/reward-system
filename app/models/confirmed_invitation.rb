class ConfirmedInvitation

  attr_reader :invited_by, :accepted_by, :invited_at

  class << self
    attr_reader :confirmed_invitations
  end

  def initialize(invited_by: , accepted_by: , invited_at:)
    @invited_by = invited_by
    @accepted_by = accepted_by
    @invited_at = invited_at
  end

  def self.init
    @confirmed_invitations = []
  end

  def self.all
    @confirmed_invitations
  end

  def delete
    ConfirmedInvitation.all.delete(self)
  end

  def self.add(invitation)
    confirmed_invitation = all.find { |invt| invt.accepted_by == invitation.invited_to }
    if confirmed_invitation && invitation.invited_at.to_time < confirmed_invitation.invited_at.to_time
      confirmed_invitation.delete
    end
    @confirmed_invitations << new(
      invited_by: invitation.invited_by,
      accepted_by: invitation.invited_to,
      invited_at: invitation.invited_at,
     )
  end

end
