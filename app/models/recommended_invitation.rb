class RecommendedInvitation

  attr_reader :invited_by, :invited_to, :invited_at

  def initialize(invited_by:, invited_to:, invited_at:)
    @invited_by = invited_by
    @invited_to = invited_to
    @invited_at = invited_at
  end

  def accepted?
    AcceptedInvitationSoFar.all.find { |acc_invt| acc_invt.accepted_by == invited_to }.present?
  end

end
