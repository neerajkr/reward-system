class RewardCalculator

  def initialize(file:)
    @file = file
  end

  def calculate
    parsed_data = InputParser.new(file: @file).parse
    AcceptedInvitationSoFar.init
    ConfirmedInvitation.init
    parsed_data.reverse.each do |invitation|
      AcceptedInvitationSoFar.add(invitation) if invitation.is_a?(AcceptedInvitation)

      ConfirmedInvitation.add(invitation) if invitation.is_a?(RecommendedInvitation) && invitation.accepted?
    end

    response
  end

  private

  def rewarding(n)
    n == 0 ? 1.0 : (0.5**n + rewarding(n - 1))
  end

  def response
    rewards = {}
    ConfirmedInvitation.all.each_with_index { |h, index| rewards[h.invited_by] = rewarding(index) }
    rewards.sort.to_h
  end
end
