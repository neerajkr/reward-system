class AcceptedInvitationSoFar

  class << self
    attr_reader :accepted_invitations
  end

  def self.init
    @accepted_invitations = []
  end

  def self.all
    @accepted_invitations
  end

  def self.add(invitation)
    all << invitation
  end
end
